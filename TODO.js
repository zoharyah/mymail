// Shaul questions:
// ================
// in runServer.js:93-102
//  remove loginFail completely, and instead of definning loginSucc and then activating it just run lines 94-96?

// in myMailUserDB we update email status (read/unread, delete etc.) by (mailID, username)
// in runServer we call these methods with (req.body, username), do we set the req.body to be the emailID somewhere?
// this might be the problem with updates not saving on the database without setting id to 1

// in show.js:60-68
//  is initialShow needed? if so change name of '#todoshow'

// is composeMailView.js needed?


// in mailCollection.js
// app.MailCollec.remove(model); is called twice, once in line 106 and once in 118



ZOHAR:
disable logout animation
remove console log in myHTTP
re-add catch error in myHTTP
remove 'compose' and 'send temp mail' buttons

// Server side:
// ===========

// TODO: setup session enforcement
// TODO: socket.io
// TODO: key expire time in redis doesn't seem to be working..
// TODO: remove url lib

Done:
==========



// Client side:
// ===========

// TODO: reoccupy the Inbox and Outbox with routing - fetch mails again - might not need this with socket.io
// TODO: update server when deleting mail
// TODO: messages from different users are saved locally, need to seperate them - when working with offline/storage
// TODO: need to update server db on every change - including unread to read and so on... - possibly with socket.io
// TODO: refresh the session on navigation
// TODO: switch to using the backbone sync functionality
// TODO: issue: refresh when on the sent tab - comes up wierd
// TODO: in mailApp.js:75 change alert, maybe use '.alert#mail-sent'

Done:
==========
// TODO: change method to POST
// TODO: indicate if message is read or not
// TODO: maybe add test to see if already logged in - if so skip the welcome page
// TODO: update count when deleting mail


// Both:
// ====

// TODO: push new mails to clients