// private cookie API

// define a Cookie constructor 
function Cookie(key, value, expires){
	this.key = key;
	this.value = value;
	this.expires = expires;
}

// get string out of cookie object
Cookie.prototype.toString = function (){
	var string = this.key + '=' + this.value + '; ';
	string += (!this.expires)? '' : 'Expires=' + this.expires + '; ';
	string += 'Path=/; ';
	return string;
}

// convert request cookie to object
function getRequestCookie(data) {
	
	var cookieObj = {},
			attrValArr = [];
			
	if(!data)
		return data;
		
	var cookie = data.trim().split(/[\s;:]+/);
	for (var i in cookie){
		attrValArr = cookie[i].split(/=/);
		cookieObj[attrValArr[0]] = attrValArr[1];
	}
	return cookieObj;
}

exports.Cookie = Cookie;
exports.getRequestCookie = getRequestCookie;