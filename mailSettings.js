/**
 * constatns for myMail
 */

exports.SESSION_TIMEOUT_MIN = 30;
exports.MIN_TO_MISC = 6000;
exports.SESSION_TIMEOUT_SECS = 60 * 5; // 5 minutes
exports.COOKIE_EXPIRATION = 24 * 60 * 60 * 1000;


exports.GLOBAL_MAIL_APP_PAGE_LOC = '/mail.html';
exports.GLOBAL_LOGIN_REG_PAGE_LOC = '/welcome.html';
exports.MAIL_APP_PAGE_LOC = './public/resources/mail.html';
exports.LOGIN_REG_PAGE_LOC = './public/resources/welcome.html';