var parseRequest = require('./zoharyah_httpParser'),
		responseModule = require('./response'),
		net = require('net'),
		fs = require('fs'),
		path = require('path'),
		consts = require('./settings'),
		url = require('url'),
		util = require('util'),
		emitter = require('events').EventEmitter;

//http://localhost:8888/

//=============================================================


var myServer;

function Server(root) {
	this.server;
	this.stopped;
	this.rootPath = root;
	this.port = 'N/A';
	this.startDate = 'N/A';
	this.numOfCurrRequests = 0;
	this.totalSuccRequests = 0;
	this.totalFailedRequests = 0;
	this.isStarted = false;
	this.listeners = [];
}


// set Server to extend the eventEmitter object
util.inherits(Server, emitter);

// callback function for updating this servers stats according to received status
var updateStatusCB = function updateStatusCB(status) {
	myServer.numOfCurrRequests--;

	//only status consts.STATUS_OK is considered success
	if (status === consts.STATUS_OK)
		myServer.totalSuccRequests++;
	else
		myServer.totalFailedRequests++;
		
	// last one closes the lights
	if(myServer.stopped && !myServer.numOfCurrRequests)
		myServer.stop();
}

Server.prototype.start = function start(port) {

	var _statusHandler,
			_this = this;

	//create server with the following cb function:
	this.server = net.createServer(function(socket) {
		var _response,
				_req,
				_handleData,
				_requestAccum = '',
				_keepAlive = false;
    
    // our server can register as many listeners as he wants for each method(=event)
    process.setMaxListeners(consts.UNLIMITED_LISTENERS); 

		process.on('uncaughtException', function(err) {
		  console.log('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
			if(_response && _response.counted){
			  _response.status = consts.STATUS_INTERNAL_SERVER_ERROR;
			  _response.end();
				}
		  console.log('Server had an unexpected error: ' + err);
		  console.log('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
		});
				
		// if this server should stop, don't handle this connection
		if(!_this.isStarted || _this.stopped)
			return;

		// set timeout for that connection
		socket.setTimeout(consts.LAST_REQUEST_TIMEOUT_SEC * consts.SECOND, function() {
			
			// if _keepAlive === true, timeout event doesn't apply, ignore it
			if (!_keepAlive){
				// console.log('connection timed out');
				if(!_response){
  				_response = new responseModule.Response(socket, updateStatusCB, function(){myServer.emit('responseCreated');});
				}
				_response.status = consts.STATUS_TIME_OUT;
				
				//close connection after writing time out response
				_response.closeConnection = true;
				_response.end();
				
				// let it be known - this connection is closed
				_response.alive = false;
			}
		});
		
		socket.setEncoding('utf8');

//================================================INCOMING_REQUESTS_HANDLER=============================================
		//data handler for that socket
		_handleData = function handleData(data) {
			var _bodyLength;
			
			if(myServer.stopped){
  			if(!_response){
          //create a new response object for that new request
          _response = new responseModule.Response(socket, updateStatusCB, function(){myServer.emit('responseCreated');});
  			}
        _response.status = consts.STATUS_SERVICE_UNAVAILABLE;
        //close connection after writing time out response
        _response.closeConnection = true;
        _response.end();
        // let it be known - this connection is closed
        _response.alive = false;
        return;
  	  }
  	  //TODO: remove
  	  // console.log(data)

			// accumulate the request in case it comes in several chuncks
			_requestAccum += data;

			// if haven't recieved all headers yet - wait for rest of message
			if (_requestAccum.indexOf(consts.END_OF_HEADERS) < 0)
				return;

			// recieved all headers, and havn't parsed message yet, parse it now
			if (!_req) {
				
				//create a new response object for that new request
				_response = new responseModule.Response(socket, updateStatusCB, function(){myServer.emit('responseCreated');});
        
				// parse request (or at least its headers)
				_req = parseRequest(_requestAccum);

				//if request status entry is defined, request parsing was unsuccessful
				if (_req.status) {

					//update status for response, according to parse failure reasons
					_response.status = _req.status;
					_response.end();

					// prepare for next request on that connection
					_req = undefined;
					_response = undefined;

					// see if got more than the current failed request if so, restart that method with that new request
					if (data.length !== _requestAccum.length) {
						_requestAccum = '';
						_handleData(data);
					}
					_requestAccum = '';
					return;
				}

				//if got a request with a connection keep-alive header, keep it alive
				if (_req.headers.Connection === 'Keep-Alive') {
					_response.closeConnection = false;
					_keepAlive = true;
				}

				//if got a request with a connection close or http version 1.0 without a keep-alive, don't keep it alive
				else if (_req.headers.Connection === 'close' || _req.version.match(/( )*HTTP\/1\.0( )*/)) {
					_response.closeConnection = true;
					_keepAlive = false;
				}

				// if didn't get content-length header in request, add it with value of 0
				if (!_req.headers[consts.CONTENT_LENGTH_HEADER])
					_req.headers[consts.CONTENT_LENGTH_HEADER] = '0';
			}

			// already parsed the headers, got rest of body
			else {
				_req.body += data;
			}
			_bodyLength = parseInt(_req.headers[consts.CONTENT_LENGTH_HEADER]);


			// if got all of body according to content-length, start working on a response
			if (_req.body.length >= _bodyLength) {
				_requestAccum = '';

				// see if got more that just that current request's body, if so, keep it as the next request to be handled
				_requestAccum = _req.body.substr(_bodyLength);
				_req.body = _req.body.substr(0, _bodyLength);

				//handle full request
				httpRequestHandler(_req, _response);
				_req = undefined;

				// handle next request
				if (_requestAccum.length > 0)
					_handleData('');
			}
		}; //handleData
//================================================END_INCOMING_REQUESTS_HANDLER=============================================

		// register data handler
		socket.on('data', _handleData);

		socket.on('error', function(e) {}); 

	});//createServer

	// have server start to listen to given port on loclhost
	this.server.listen(port);

	// start server starts when it starts listening
	this.server.on('listening', function() {
		_this.isStarted = true;
		_this.stopped = false;
		_this.port = port;
		_this.startDate = new Date().toString();
		_this.numOfCurrRequests = 0;
		_this.totalSuccRequests = 0;
		_this.totalFailedRequests = 0;
		_this.emit("start");
	});
	
	this.on('responseCreated', function(){myServer.numOfCurrRequests++;});

	// handle server errors
	this.server.on('error', function(e) {
		if (e.code === 'EADDRINUSE') {
			console.log('port is already in use.');
			console.log('please try again with a different port.');
		}
		else console.error(e);
	});
	
	// dynamic status request handler
	_statusHandler = function statusHandler(request, response, params){
		response.status = consts.STATUS_OK;
		response.headers[consts.CONTENT_TYPE_HEADER] = 'text/plain';
		response.end(JSON.stringify(myServer.status()));
	}

	// register dynamic status request
	this.get('/status', {call:_statusHandler});
	this.post('/status', {call:_statusHandler});
}

// generate server status report
Server.prototype.status = function status() {

	// used to solve devision by zero problem
	var _denominator,
			_percentage;
			
	_denominator = this.totalSuccRequests + this.totalFailedRequests;
	_percentage = this.totalSuccRequests * 100 / ( _denominator ? _denominator : 1);
	
	return {
		isStarted : this.isStarted,
		startDate : this.startDate,
		port : this.port,
		numOfCurrentRequests : this.numOfCurrRequests,
		
		// if there were no successfull requests but also no failed requests - its a 100 %
		precntageOfSuccesfulRequests : _denominator ? _percentage : 100
	};
}

/*
 * fetchResource recives a parsed httpRequest object
 * validates that the requested resource is under the root folder (for security reasons)
 * returns the path to the resource and it's type if validation passes and false otherwise
 * This function maintaines the server's file hierarchy: seperation depending on file types to ensure only supported types are available
 */
var fetchResource = function fetchResource(request) {
	var _type,
			_url, 
			_resName, 
			_ext, 
			_path, 
			_folder = myServer.rootPath + '/resources';

	_url = url.parse(request.uri);
	_resName = _url.pathname.substr(_url.pathname.lastIndexOf('/'));
	_ext = path.extname(_resName).toLowerCase();
	
	// if requested resource doesn't have an extention - this is not a valid static resource request
	if(!_ext.length)
		return false;
	
	_path = _folder + request.resPath;

	// make sure that desired file is under the root directory
	if (path.resolve(_path).indexOf(path.resolve(process.cwd()+'/'+myServer.rootPath)) !== 0) {
		// console.log('security breach');
		return false;
	}
	
	// if requested resource is of supported file types - return its path and type
	if(consts.extensionTypes[_ext]){
		return {
			path : _path + _resName,
			type : consts.extensionTypes[_ext]
		};
	}
	
	// otherwise, return false
	return false;
}

/*
 * handling of static requests, gets a request and a response objects
 * updates the response object according to the request
 */
var myStaticRequestHandler = function myStaticRequestHandler(request, response) {
	var _pathAndType;

	// check if resource is available for the client
	if ( _pathAndType = fetchResource(request)) {

		fs.exists(_pathAndType.path, function(exists) {
			if (exists) {

				// resource is available for the client
				fs.readFile(_pathAndType.path, function(err, data) {
					if (err) {

						//send error response
						console.log(err);
						response.status = consts.STATUS_INTERNAL_SERVER_ERROR;
						response.end();
						return;
					}

					//managed to read resource, sending it back to the client
					response.status = consts.STATUS_OK;
					response.headers[consts.CONTENT_TYPE_HEADER] = _pathAndType.type;
					response.end(data);
				});
			} else {
				//resource doesn't exist, send response
				response.status = consts.STATUS_NOT_FOUND;
				response.end();
			}
		});
	}
	// we got a security breach, or a request for an unsupported type
	else {
		response.status = consts.STATUS_NOT_FOUND;
		response.end();
	}
}

/**
 * stop() stops this server once all current connections are closed 
 */
Server.prototype.stop = function stop() {
	
	this.stopped = true;
	if(!this.numOfCurrRequests && this.isStarted){
		this.isStarted = false;
		this.server.close(function(){console.log('closed server')});
	}
}

//=========================================DYNAMIC===================================================

/**
 * emitResource recieves a request and a response,
 * looks for a listener listening to a string in the format of the URI of the given request
 * if so, creates matching params object, and emits an event with it for the relavent listener
 * otherwise, return false 
 */
var emitResource = function emitResource(request, response){
	var rInd,
			fInd,
			listenInd,
			format,
			resource;
			paramId = '',
			paramVal = '',
			params = {};
		
	resource = request.method+url.parse(request.uri).pathname;
			
	// iterate over registered listener's format strings
	for(listenInd=0; listenInd < myServer.listeners.length; listenInd++){
		format = myServer.listeners[listenInd];

    // iterate over current format string and resource string at the same time, to see if they match
		for(rInd=0, fInd=0; rInd < resource.length, fInd < format.length; rInd++, fInd++){
			if(format[fInd] === resource[rInd])
				continue
				
			// reached a param name in format string, save param name from format string, and value from resource
			if(format[fInd] === ':'){
				fInd++;
				
				// save param name
				for(; rInd < resource.length && resource[rInd] !== '/'; rInd++){
					paramVal += resource[rInd];
					
				// save param value
				}for(; fInd < format.length && format[fInd] !== '/'; fInd++){
					paramId += format[fInd];
				}
				
				// save param name and value in params object
				params[paramId] = paramVal;
				paramId = '';
				paramVal = '';
				
				// if reached end of both strings - format and resource, we're done
				if(rInd === resource.length && fInd === format.length)
				  break;
			}
			else{
			  
			  // strings don't match, reinit params, and try with next format string
				params = {};
				break;
			}
		}
		
		// if reached end of both strings - they match and we can emit the event
		if(rInd === resource.length && fInd === format.length){
			return myServer.emit(format, request, response, params);
		}
	}
	return false;
}

/**
 * onStart(callback) recieves a callback function, and calls it once a 'start' event is emitted 
 */
Server.prototype.onStart = function onStart(callback){
	this.on("start", callback);
}	

/**
 * get(resource, cb) registers a dynamic get request handler - cb, for the resource string resource
 */
Server.prototype.get = function get(resource, cb){
	var regString = consts.GET_METHOD + resource;
	
	// save registered string
	myServer.listeners.push(regString);
	
	// call this cb function when regString event is emitted
	this.on(regString, function(request, response, params){cb.call(request, response, params);});
}

/**
 * post(resource, cb) registers a dynamic post request handler - cb, for the resource string resource
 */
Server.prototype.post = function post(resource, cb){
	var regString = consts.POST_METHOD + resource;
  
  // save registered string
	myServer.listeners.push(regString);
  
  // call this cb function when regString event is emitted
	this.on(regString, function(request, response, params){cb.call(request, response, params);});
}

// handler for http requests, call appropriate handler according to method
var httpRequestHandler = function httpRequestHandler(request, response) {
	
	// initiate response timeout
	response.setResponseTimeout();

  // check if there is a dynamic request handler registered for this request
	if(emitResource(request, response))
		return;

  // no dynamic request handler for this request, proceed with static request handling
	// make sure method is either GET or POST
	switch(request.method) {
		case consts.GET_METHOD:
		case consts.POST_METHOD:
			myStaticRequestHandler(request, response);
			break;
		default:
			response.status = consts.STATUS_METHOD_NOT_ALLOWED;
			response.end();
			return;
	}
}

//==================================================================================

/**
 * createHTTPServer(rootFolder) creates a web server but does not start to listen.
 * rootFolder is the server's root folder.
 */
function createHTTPServer(rootFolder) {
  myServer = new Server(rootFolder);
	return myServer;
}

module.exports.createHTTPServer = createHTTPServer;
