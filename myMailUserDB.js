var mongoose = require('mongoose'),
    uuid = require('node-uuid'),
		mongoUrl = 'mongodb://localhost:27017',
		_ = require('underscore');


// ============================MAIL USERS DATABASE==========================

// scheme definition for the user database
// each user has a unique user (name)
var User = mongoose.Schema({ 
		user: {
			      type: String,
			    	required: true,
			    	unique: true
  				},
		pass: {
			      type: String,
			    	required: true
  				},
		firstName:	{
									type: String,
									required: true
								},
		lastName:	{
								type: String,
								required: true
							},
		age:	{
						type: Number,
						required: true
					},
		sessionID: {
						      type: String,
						    	required: true,
						    	unique: true
			  				},
		mails : [{ 
								subject: String,
								userName: String,
								firstName: String,
								lastName: String,
								messageBody: String,
								time: Date,
								// every mail has a unique uuid to avoid duplicates
								uuid: String,
								sent: Boolean,
								read: Boolean
							}]
});

var UserModel = mongoose.model('User', User);
mongoose.connect(mongoUrl);

function UserDB(){
	this.userSockets = {};
}

/**
 * add a new user to database
 */
UserDB.prototype.addUser = function addUser (params, sessionID, success, fail) {
	
	// create new user
	var newUser = new UserModel();
	
	// params are already validated by server, just copy them to the new user
	newUser.user = params.user;
	newUser.pass = params.pass;
	newUser.firstName = params.firstName;
	newUser.lastName = params.lastName;
	newUser.age = params.age;
	newUser.sessionID = sessionID;
	
	// save user to database - call success/fail callback according to result
	newUser.save(function(err, user) {
		if (err) {
			console.log(err);
			fail();
		} else {
			success();
		}
	});
}

/**
 * check if a user exists in database, and if password is correct
 * callback is called with the result
 */
UserDB.prototype.validateUser = function validateUser (username, pass, callback) {
	
	// find user in database
	UserModel.findOne({ user: username }, function(err, fetchedUser) {
		if (err){
			callback(false);
		}
		else {
			if (!fetchedUser){
			  // user was not found
				callback(false);
			}
			else {
			  // found user, compare password
			  callback(fetchedUser.pass === pass);
			}
		}
	});
}

/**
 * update database for sending a mail
 * handle both sender and recipient sides
 * updates user of new mail using socket.io
 */
UserDB.prototype.sendMail = function sendMail (mail, username, success, fail){
	
	var sentTo = mail.userName,
			_this = this;
	
	// validate args are defined
	if(!mail || !username){
		fail();
	}
	
	// find sender in database
	UserModel.findOne({ user: username }, function(err, senderUser) {
		if(err || !senderUser){
      // user was not found
      console.log(err);
      fail();
	  }

    // set mail parameters for recipient side (details of sender)
		mail.sent = false;
		mail.userName = username;
		mail.uuid = uuid();
	  mail.firstName = senderUser.firstName;
	  mail.lastName = senderUser.lastName;
	  
	  // find recipient in database
		UserModel.findOne({ user: sentTo }, function(err, recieverUser) {
	    if(err || !recieverUser){
        // user was not found
	      fail();
	    }
	    else{
		    // add mail to recieving party's mail list
	    	recieverUser.mails.push(mail);
	    	recieverUser.save(function(){
	        if (_this.userSockets[sentTo]){
	          // notify recipient of new mail
	        	_this.userSockets[sentTo].emit('newMail');
	        }
					
					// set mail parameters for sender side (details of recipient)
					mail.uuid = uuid();
					mail.userName = sentTo;
					mail.firstName = recieverUser.firstName;
					mail.lastName = recieverUser.lastName;
					mail.sent = true;
					mail.read = true;
					
					// add mail to sender mail list
					senderUser.mails.push(mail);
					senderUser.save(function(){
						if (_this.userSockets[username]){
						  // notify sender that the mail was sent
		        	_this.userSockets[username].emit('mailSent');
		        }
		        success();
					});
				});
			}
		});
	});
}
	
/**
 * delete mail from user mail list
 */
UserDB.prototype.deleteMail = function deleteMail (mailID, username){
	
	var index;
	
	// validate args are defined
	if(!mailID || !username){
		return;
	}
	
	// find user in database
	UserModel.findOne({ user: username }, function(err, fetchedUser) {
		if (err){
			console.log(err);
			return;
		}
		else {
			if (!fetchedUser){
        // user was not found
				return;
			}
			else {
				for (index in fetchedUser.mails){
				  // go over mail-list and find desired mail
					if(fetchedUser.mails[index].uuid === mailID){
					  // remove mail from mail-list
						fetchedUser.mails.splice(index, 1);
						fetchedUser.save();
						return;
					}
				}
			}
		}
	});
}

/**
 * toggel read/unread status of a mail in db
 */
UserDB.prototype.toggleReadMail = function toggleReadMail (mailID, username){
	
	var index;
	
	// validate args are defined
  if(!mailID || !username){
		return;
	}
	
	// find user in database
	UserModel.findOne({ user: username }, function(err, fetchedUser) {
		if (err){
			console.log(err);
			return;
		}
		else {
			if (!fetchedUser){
        // user was not found
				return;
			}
			else {
				for (index in fetchedUser.mails){
				  // go over mail-list and find desired mail
					if(fetchedUser.mails[index].uuid === mailID){
					  // toggel read/unread flag
						fetchedUser.mails[index].read = !fetchedUser.mails[index].read;
						fetchedUser.save();
						return;
					}
				}
			}
		}
	});
}

/**
 * get a user's entire mail-list
 * upon success calls success() with user's mails
 */
UserDB.prototype.fetchUserMails = function fetchUserMails (username, success, fail){
  
  // find user in database 
	UserModel.findOne({ user: username }, function(err, fetchedUser) {
		if (err){
			fail();
		}
		else {
			if (!fetchedUser){
			  // user was not found
				success([]);
			}
			else {
			  // return user's mail-list
			  success(fetchedUser.mails);
			}
		}
	});
}

// =================DEBUG================
/**
 * get entire user database
 */
UserDB.prototype.getDB = function getDB (getDBcb){
	UserModel.find({}, function(err, users) { getDBcb(users); });
}

/**
 * clear entire user database
 */
UserDB.prototype.clearDB = function clearDB (){
	UserModel.find({}, 
	  function(err, users) {
	    // for each user in database - remove it
	    _.each(users, function(u) { u.remove(); });
	  }
	);
}
// =================END_DEBUG============

//exports
exports.userDB = new UserDB();
