/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	// mail Collection
	// ---------------

	var MailList = Backbone.Collection.extend({
		// Reference to this collection's model.
		model: app.mailModel,

    // get sent mails
		sent: function () {
			return this.getMails('sent', true);
		},

		// mails are sorted by their send dates - newest mails first.
		comparator: function (mail) {
			return mail.get('date');
		},
		
		// get mails where attribute = value
		getMails: function(attribute, value) {
			return this.filter(function (mail) {
				return mail.get(attribute) === value;
			});
		},
		
		// remove a specified mail from the mail collection
		remove: function(model) {
			var i;
			for(i in app.MailCollec.models){
			  // check each mail in collection
				if(app.MailCollec.models[i] === model){
				  // if desired mail remove it
					app.MailCollec.models.splice(i, 1);
					// update inbox/outbox title
					this.trigger('updateMailHeader');
				}
			}
		},
		
		// fetch mails for this user from the server
		fetch: function() {
			var index;
			
			$.ajax({
				type : 'GET',
				dataType : 'json',
				data: '',
				url: window.location.origin + '/mymail/fetchMail',
				error: function(e){console.log('got an error in fetching mails....' + e);}
			}).done(function ( data ) {
					
				  // add mails to collection
				  for(index in data){
				  	// is there is no other mail in collection with the same uuid - add it to collection
				  	if ( ! app.MailCollec.where({uuid: data[index].uuid}).length ){
					  	data[index].time = new Date(data[index].time).toLocaleString();
							data[index].messageBody = data[index].messageBody.replace(/\n/g, '<br />');
					  	app.MailCollec.create(data[index]);
							app.MailCollec.trigger('filterAll');
					  }
				  }
			});
		}
		
	});
	
	// overrides Bacckbone.sync, used to update server on client actions
	// create, read, delete or update	
	Backbone.sync = function(method, model, options) {
		var i,
				data = model.attributes.uuid;
				
		// update inbox/outbox title
    app.MailCollec.trigger('updateMailHeader');
		model.id = 1;
		
    switch (method) {
		  case 'update':
        if (model._previousAttributes.read !== model.attributes.read){
          // need to update read/unread status
					method = 'toggleRead';
				}
				// extend user's session timeout - decided to remove this to save on network traffic
				// else method = 'probe';
				else return;
				break;
      case 'delete':
			  // remove mail from local collection
				app.MailCollec.remove(model);
				break;
			default:
				return;
		}
		// send server the needed action
		$.ajax({
				type : 'POST',
				data: data,
				url: window.location.origin + '/mymail/' + method
			}).fail(function (data){
				console.log('got an error in sync....' + JSON.stringify(data));
			});
  }

	// Create our global collection of **mails**.
	app.MailCollec = new MailList();
})();
