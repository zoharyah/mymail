/*global $ */
/*jshint unused:false */
var app = app || {};
var ENTER_KEY = 13;

$(function () {
	'use strict';

	// kick things off by creating the `App`
	new app.AppView();
	new app.MailWindowView();
});


/**
 * logout button click handler
 * end session and redirect to welcome (sign in) page
 */
var logOut = function logOut() {
		$.ajax({
			type : 'POST',
			contentType: "application/json",
			url: window.location.origin + '/mymail/endSession'
		}).always(function(){
			document.location.href = '/welcome.html';
		});
		return false;
}

/**
 * isMessageApplicable returns true if message can be sent - it conaines valid characters
 */
var isMessageApplicable = function isMessageApplicable(){
	var regexp = /^[ a-zA-Z\u0000-\u024F]*$/i;
	return (regexp.test(jQuery.trim($('#compose-to').val())) && regexp.test(jQuery.trim($('#compose-subject').val())) && regexp.test(jQuery.trim($('#compose-body').val())));
}

/* 
 * send mail handler - sends new mail to server
 * if successful - go to inbox
 * if fails - show message to user
 */
var sendMail = function sendMail () {
	var $composeSubj = $('#compose-subject'),
			$composeTo = $('#compose-to'),
			$composeBody = $('#compose-body'),
			$sendError = $('#send-error');
	
	// if message contents are not applicable for sending - show error and return false		
	if(!isMessageApplicable()){
		$('#send-error-content').fadeIn();
		return false;
	}
	
	//request send mail from server
	$.ajax({
		type : 'POST', //post
		contentType: "application/json",
		data: JSON.stringify({
		  // extract mail data from document
		  subject: $composeSubj.val(),
			userName: $composeTo.val(),
			messageBody: $composeBody.val(),
			time: new Date()
		}),
		url: window.location.origin + '/mymail/send'
	}).fail(function(){
		$sendError.fadeIn();
	}).done(function() {
    // on success reset compose screen
    $sendError.fadeOut();
		app.MailRouter.navigate('/#');
		$composeSubj.val('');
		$composeTo.val('');
		$composeBody.val('');
	 });
	return false;
}

/**
 * get a cookie (will be used to retrive the user's session id)
 */
var getCookie = function getCookie(cookieName) {
	var cookieStart,
			cookieEnd;
  if (document.cookie.length > 0) {
    // look for cookie with cookieName  
    cookieStart = document.cookie.indexOf(cookieName + "=");
    if (cookieStart != -1) {
      cookieStart = cookieStart + cookieName.length + 1;
      // look for cookie's end
      cookieEnd = document.cookie.indexOf(";", cookieStart);
      if (cookieEnd == -1) {
        // it's the last cookie 
        cookieEnd = document.cookie.length;
      }
      return unescape(document.cookie.substring(cookieStart, cookieEnd));
    }
  }
  return "";
}


/*
 * set up mail app
 */
$(window).on('ready', function() {
	var $composeMail = $('#compose-mail');
	
	// set up socket.io - listen for new mail
	app.socketIO = io.connect(window.location.protocol + '//' + window.location.hostname + ':' + (parseInt(window.location.port)+2));
	app.socketIO.emit('startSession', { UserName: getCookie('UserName') });

	$('#user-name').html(getCookie('UserName'));
	
	// handle "send mail" event (triggered from the compose screen) - empty subjects and bodies are allowed
	$composeMail.submit(sendMail);
	
	// bind logout button to logOut function
	$('#logout-btn').click(logOut);
	
  // handle "new mail recived" event - socket.io
  app.socketIO.on('newMail', function () {
    // show notification
    $('.alert#new-mail').show().animate({
			'right': '0'
		}, 'fast');
		setTimeout(function(){
			$('.alert#new-mail').animate({
				'right': '-50px'
			}, 'fast', function(){$(this).hide();});
		}, 5000);
		// get new mail (update inbox)
    app.MailCollec.fetch();
  });
  
  // handle "mail sent" event - socket.io
  app.socketIO.on('mailSent', function () {
    // show notification
    $('.alert#mail-sent').show().animate({
			'left': '0'
		}, 'fast');
		setTimeout(function(){
			$('.alert#mail-sent').animate({
				'left': '-50px'
			}, 'fast', function(){$(this).hide();});
		}, 5000);
		// get sent mail (add to outbox)
    app.MailCollec.fetch();
  });
	
});