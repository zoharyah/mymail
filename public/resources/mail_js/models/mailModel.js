/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	// mail Model
	// ----------

	app.mailModel = Backbone.Model.extend({
		// Default attributes for a mail
		defaults: {
			
			// is email shown in mail window
			shown: false,
			subject: '',
			userName: '',
			firstName: '',
			lastName: '',
			messageBody: '',
			// when was the mail sent
			time: '',
			// a unique id
			uuid: '',
			// is it an outgoing(true) or an incoming(false) mail
			sent: false,
			// is email read already
			read: false
		},

		// Toggle the `read` state of this mail.
		toggleRead: function () {
			this.save({
				read: !this.get('read')
			});
		},
		
		// unshow this mail if its shown
		unShow: function () {
			if(this.get('shown')){
				this.save({
					'shown': false
				});
			}
		}
	});
})();
