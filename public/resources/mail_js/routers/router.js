/*global Backbone */
var app = app || {};

(function () {
	'use strict';

	// myMail Router
	// ----------
	// rout between different parts of the application using some animation eye-candy
	var Workspace = Backbone.Router.extend({
		routes: {
			'': 'recieved',
			'compose': 'compose',
			'inbox': 'recieved',
			'sent': 'sent',
			'recieved': 'recieved'
		},
		
		// inbox rout: show "inbox" screen
    sent: function(){
			var _this = this,
					$mailTransbox = $('.mail-transbox'),
					$transbox = $('.transbox'),
					$navli = $('.nav li'),
					$sentNav = $('#sent-nav');
			
			app.filterMode = 'sent';
			$transbox.css({'overflow-y': 'hidden'});
			$navli.removeClass('active');
			$sentNav.addClass('active');
			$mailTransbox.show().animate({
	      'left': '-32%',
	    }, 'fast', function(){
				app.MailCollec.trigger('filterAll');
				_this.defaultRout();
		    $mailTransbox.show().animate({
		      'left': '0',
		    }, 'fast');
	    });
		},
		
		// outbox rout: show "outbox" screen
    recieved: function(){
			var _this = this,
					$mailTransbox = $('.mail-transbox'),
					$transbox = $('.transbox'),
					$navli = $('.nav li'),
					$inboxNav = $('#inbox-nav');
			
			app.filterMode = 'recieved';
			$transbox.css({'overflow-y': 'hidden'});
			$navli.removeClass('active');
			$inboxNav.addClass('active');
			$mailTransbox.show().animate({
				'left': '-32%',
	    }, 'fast', function(){
				app.MailCollec.trigger('filterAll');
				_this.defaultRout();
		    $mailTransbox.show().animate({
		    	'left': '0',
		    }, 'fast');
	    });
		},
		
		// default rout: for any rout that's not compose rout
		defaultRout: function() {
			var $mailTransbox = $('.mail-transbox'),
					$compose = $('.compose-mail-section'),
					$transbox = $('.transbox'), 
					$sendError = $('.mymail-error');
			app.MailCollec.trigger('updateMailHeader');
			$mailTransbox.show().animate({
	    	'top': '0',
	    }, 'fast');
			$compose.animate({
	    	'top': '-110%',
	    }, 'fast', function() {
		    $compose.hide();
		    $transbox.css({'overflow-y': 'auto'});
				$sendError.fadeOut();
		  });
		},
		
		// compose rout: show "compose new mail" screen
		compose: function () {
			var $mailTransbox = $('.mail-transbox'),
					$transbox = $('.transbox'),
					$composeSection = $('.compose-mail-section'),
					$navli = $('.nav li'),
					$composeNav = $('#compose-nav');
					
			$transbox.css({'overflow-y': 'hidden'});
			$navli.removeClass('active');
			$composeNav.addClass('active');
			$mailTransbox.animate({
	      'top': '100%',
	    }, 'fast', function() {
		    $mailTransbox.hide();
		  });
			$composeSection.show().animate({
	      'top': '0',
	    }, 'fast', function(){
		    $transbox.css({'overflow-y': 'auto'});
	    });
		}
	});

	app.MailRouter = new Workspace();
	Backbone.history.start();
})();
