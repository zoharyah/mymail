/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {
	'use strict';

	// The Application
	// ---------------

	// Saving the global app view.
	app.AppView = Backbone.View.extend({

		// Instead of generating a new element, bind to the existing skeleton of
		// the App already present in the HTML.
		el: '#mail-inbox-list',

		initialize: function () {
			this.$main = this.$('#main');
      
      // register listeners
			this.listenTo(app.MailCollec, 'add', this.addOne);
			this.listenTo(app.MailCollec, 'change:completed', this.filterOne);
			this.listenTo(app.MailCollec, 'filterAll', this.filterAll);
			this.listenTo(app.MailCollec, 'updateMailHeader', this.updateMailHeader);

			// populate with user's mails
			app.MailCollec.fetch();
		},

		/** 
		 * Add a single mail to the list by creating a view for it, and
		 * prepending its element to the `<ul>`.
		 */
		addOne: function (mail) {
			var view = new app.MailView({ model: mail });
			this.$el.prepend(view.render().el);
		},

    /**
     * trigger 'filter' on a single mail (filtering incoming/outgoing)
     */
		filterOne: function (mail) {
			mail.trigger('filter');
		},

    /**
     * trigger 'filter' on all mails (filtering incoming/outgoing)
     */
		filterAll: function () {
		  // for each mail run filterOne
			app.MailCollec.each(this.filterOne, this);
		},
		
		/**
		 * update Inbox/Outbox title - count of mails in the category
		 */
		updateMailHeader: function () {
			$('#mails-title').html((app.filterMode !== 'sent')? 'Inbox' : 'Outbox');
			$('#mails-title-count').html('(' + app.MailCollec.getMails('sent', (app.filterMode === 'sent')).length + ')');
		}
	});
})(jQuery);
