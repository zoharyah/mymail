/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function($) {
	'use strict';

	// MailWindowView
	// --------------

	// The DOM element for the MailWindowView...
	app.MailWindowView = Backbone.View.extend({

		el : ".inner-transbox-right",

		// Cache the template function for a single item.
		template: _.template($('#show-template').html()),

		initialize : function() {
			
			// set up listeners:
			
			//render on every change in shown field - to update mail window view accordingly
			this.listenTo(app.MailCollec, 'change:shown', this.render);
			
			//when a mail is removed, while it's shown, hide the show aswell
			this.listenTo(app.MailCollec, 'remove', (function(mail) {
				//if the mail that is removed is the one that's shown
				if(mail.get('shown')){
				  // show "empty" screen instead of that mail
					$('#chosen-email-not-empty').hide();
					$('#chosen-email-empty').show();
				}
			}));
		},

    // render view according to 'shown' value of the mail
		render : function(mail) {
			if (mail.get('shown')) {
			  // show this mail
				this.$el.html(this.template(mail.toJSON()));
				$('#chosen-email-empty').hide();
			  $('#chosen-email-not-empty').show();
			}
		}
	});
})(jQuery);
