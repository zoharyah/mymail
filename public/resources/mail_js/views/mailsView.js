/*global Backbone, jQuery, _, ENTER_KEY */
var app = app || {};

(function ($) {
	'use strict';

	// single Mail View
	// --------------

	// The DOM element for a mail...
	app.MailView = Backbone.View.extend({
		tagName:  'li',

		// Cache the template function for a single mail.
		template: _.template($('#mail-template').html()),

		// The DOM events specific to a mail.
		events: {
      'click .view': 'handleMailClick',
			'click .markUnread': 'toggleRead',
			'click .reply': 'reply',
			'click .destroy': 'clear',
		},

		initialize: function () {
		  // register listeners
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
			this.listenTo(this.model, 'filter', this.filter);
			this.listenTo(this.model, 'sent', this.sent);
			this.listenTo(this.model, 'recieved', this.recieved);
			this.listenTo(app.MailCollec, 'change:read', this.render);
		},

		// Re-render the mail view.
		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			// mark as read/unread
			if (!this.model.get('read')){
  			this.$el.addClass('unread');
			}
			else {
			  this.$el.removeClass('unread');
			}
			return this;
		},
		
		// show/hide view according to global state - filterMode
		filter: function () {
			this.toggleVisible(app.filterMode === 'recieved');
		},
		
		// show inbox
		sent: function () {
			this.toggleVisible(false);
		},
		
		// show outbox
		recieved: function () {
			this.toggleVisible(true);
		},

		toggleVisible: function (sent) {
			this.$el.toggleClass('hidden', this.isHidden(sent));
		},

		isHidden: function (sent) {
			var isSent = this.model.get('sent');
			return (isSent === sent);
		},
		
		// show/unshow a mail on click
		handleMailClick: function () {
			var _this = this,
					_isSHown = this.model.get('shown'),
					$messageViewBox = $('#show-email-inner-wrapper'),
					$chosenNotEmpty = $('#chosen-email-not-empty'),
					$chosenEmpty = $('#chosen-email-empty'),
					$inboxListShown = $('#mail-inbox-list li.shown');
			
			// toggle shown flag
			this.model.save({'shown': !_isSHown});
			
			if(_isSHown){
			  // if was shown before - hide
				this.$el.removeClass('shown');
				$chosenNotEmpty.hide();
				$chosenEmpty.show();
			}
			else {
			  // if wasn't shown - show
			  
			  // mark as read
				this.model.save({'read': true});
				// make all others as not 'shown'
				app.MailCollec.each(function (mail) {
					if (mail !== _this.model)
						mail.unShow();
				});
				
				// show clicked on mail in 'shown' view
				$inboxListShown.removeClass('shown');
		  	this.$el.addClass('shown');
				$messageViewBox.show();
			}
		},
    
    // remove mail from inbox/outbox
		clear: function () {
			this.model.destroy();
			return false;
		},
		
		// toggle read/unread
		toggleRead: function () {
			this.model.toggleRead();
			return false;
		},
		
		// reply to an email
		reply: function () {
			var $composeSubj = $('#compose-subject'),
					$composeTo = $('#compose-to'),
					$composeBody = $('#compose-body');
					
			// set up compose screen to reply with wanted mail (subject, recipient and quoted data)
			$composeSubj.val('Re: ' + this.model.get('subject'));
			$composeTo.val(this.model.get('userName'));
			$composeBody.val('\n--------------------------------------------------------------------------------\n' + this.model.get('messageBody').replace(/<br \/>/g, '\r'));
			
			// move to compose screen
			app.MailRouter.navigate('/#compose');
			return false;
		}
	});
})(jQuery);
