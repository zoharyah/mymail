// custom ketchup functionality to transition between error message when navigating between login and register modes
var animateErrorsInAndOut = function animateErrorsInAndOut($in, $out) {
	$out.animate({
		top : 500,
		opacity : 0
	}, 'fast');
	$in.animate({
		top : 400,
		opacity : 0
	}, 'fast');
}

$(window).on('ready', function() {

  var $login = $('#login-nav-btn'),
      $register = $('#register-nav-btn'),
			$loginFormSec = $('.login-form-section'),
			$registerFormSec = $('.register-form-section'),
			$loginForm = $('#login-form'),
			$registerForm = $('#register-form'),
			$logUserInput = $('#login-username'),
			$logPassInput = $('#login-password'),
			$regUserInput = $('#register-username'),
			$regPassInput = $('#register-password'),
			$regPassRptInput = $('#register-password-rpt'),
			$regFirstNameInput = $('#register-first-name'),
			$regLastNameInput = $('#register-last-name'),
			$regAgeInput = $('#register-age'),
			$regBtn = $('#log-btn'),
			$logBtn = $('#reg-btn'),
			$regError = $('#register-error'),
			$logError = $('#login-error');

	// customize ketchup: add different classes for errors of different forms, and add a validation function to match passwords
	$.ketchup
		.createErrorContainer(function(form, el) {
			var elOffset = el.offset();
			return $('<div/>', {
				html : '<ul></ul><span></span>',
				'class' : 'ketchup-error ' + 'kerror-' + form.attr('id'),
				css : {
					top : elOffset.top - 5,
					left : elOffset.left + el.outerWidth() - 5
				}
			}).appendTo('body');
		})
		.validation('matchVal', 'Passwords don\'t match.', function(form, el, value, word) {
	  	return (el.val() === $(word).val());
		});
		
	// ========
	// LOGIN
	// ========
	// activate ketchup on the login form, and set submit action
	$loginForm
		.ketchup({}, {
			'#login-username' : ['required, username', 'blur'],
			'#login-password' : ['required, minlength(4)', 'blur']
		})
		.submit(function() {
			$logError.fadeOut();
			if ($(this).ketchup('isValid')) {
				$.ajax({
					type : 'POST',
					dataType : 'json',
					data: JSON.stringify({user: $logUserInput.val(), pass: $logPassInput.val()}),
					url: window.location.origin + '/mymail/login',
				  statusCode: {
				    200: function() {
							document.location.href = '/mail.html';
				  	},
				  	400: function() {
							$logError.fadeIn();
							$logPassInput.val('');
				  	}
				  },
					done : function(res) {
            // when done reset all fields
						$logUserInput.val('');
						$logPassInput.val('');
					}
				});
			}
			return false;
		});
		
	// ========
	// REGISTER
	// ========
	// activate ketchup on the registration form, and set submit action
	$registerForm
		.ketchup({}, {
			'#register-username' : ['required, username', 'blur'],
			'#register-password' : ['required, minlength(4)', 'blur'],
			'#register-password-rpt' : ['required, matchVal(#register-password)', 'blur'],
			'#register-first-name' : ['required', 'blur'],
			'#register-last-name' : ['required', 'blur'],
			'#register-age' : ['required, digits', 'blur'],
		})
		.submit(function() {
			$regError.fadeOut();
			if ($(this).ketchup('isValid')) {
				$.ajax({
					type : 'POST',
					dataType : 'json',
					data: JSON.stringify({user: $regUserInput.val(), pass: $regPassInput.val(),
						firstName: $regFirstNameInput.val(), lastName: $regLastNameInput.val(),
						age: $regAgeInput.val()}),
					url: window.location.origin + '/mymail/register',
				  statusCode: {
				    200: function() {
							document.location.href = '/mail.html';
				  	},
				  	409: function() {
							$regError.fadeIn();
							$regUserInput.val('');
				  	},
				  	400: function() {
							$regError.fadeIn();
							$regUserInput.val('');
				  	}
				  },
					done : function(res) {
					  // when done reset all fields
						$regUserInput.val('');
						$regPassInput.val('');
						$regPassRptInput.val('');
						$regFirstNameInput.val('');
						$regLastNameInput.val('');
						$regAgeInput.val('');
					}
				});
			}
			return false;
		});
		
		
// ===========================================
// register navigation click handlers for page
// ===========================================

  // handle login tab click
	$login.click(function() {
	  // if not allready in login page move to login page
		if (!$login.hasClass('active')) {

			$login.toggleClass('active');
			$register.toggleClass('active');

			$registerFormSec.hide();
			$loginFormSec.fadeIn();
			animateErrorsInAndOut($('.kerror-login-form'), $('.kerror-register-form'));
		}
		return false;
	});
	
	// handle register tab click
	$register.click(function() {
    // if not allready in register page move to register page
		if (!$register.hasClass('active')) {

			$register.toggleClass('active');
			$login.toggleClass('active');

			$loginFormSec.hide();
			$registerFormSec.fadeIn();
			animateErrorsInAndOut($('.kerror-register-form'), $('.kerror-login-form'));
		}
		return false;
	});

});
