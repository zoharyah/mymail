to install required node.js modules:
$npm install

to start the server:
$npm start

to access mail app from browser (locally):
http://localhost:1224/welcome.html


for internal use:
================
my ec2 instance's public DNS is:

ec2-54-224-19-251.compute-1.amazonaws.com

username: ubuntu


to access the mail app from browser:
http://ec2-54-224-19-251.compute-1.amazonaws.com:1224/welcome.html

the access key is in zohar's downloads folder