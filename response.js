var consts = require("./settings");

function Response(socket, cb, emitCreated) {
  this.body;
  this.headers = {};

  // default status is 200
  this.status = consts.STATUS_OK;
  this.socket = socket;
  this.alive = true;
  this.closeConnection = false;
  this.counted = true;

  // cb is called once respnse has ended
  this.serverCB = cb;

	// let server know that this response has been created with the given callback function
  emitCreated();
}

/*
 * a response will stay relevant for a preset amount of time (1 sec in our exercise)
 * this function will be called upon starting to handle the corresponding request
 * if end is not called within the timeout, response will be ended with Request Timeout (408) status
 */
Response.prototype.setResponseTimeout = function setRequestTimeout() {
  var _this = this;

  this.timeout = setTimeout(function timeoutCB() {
    _this.status = consts.STATUS_TIME_OUT;
    _this.end();
    _this.alive = false;
  }, consts.RESPONSE_END_TIMEOUT);
}

/**
 *  write(content) adds str (if given any) to te response body
 */
Response.prototype.write = function write(content) {
  if (!this.body)
    this.body = content;
  else if (content)
    this.body += content;
}

/*
 * end response adds str (if given any) to the response's body
 * recalculates content-length
 * and sends the response to the socket
 */
Response.prototype.end = function(content) {
  var _head,
      _this = this;

  // check if haven't timedout before end
  if (!this.alive)
    return;

  clearTimeout(this.timeout);

  // if havn't recieved content, override it with empty string
  content = (content === undefined) ? '' : content;
  this.body = (this.body === undefined) ? '' : this.body;

  // set current content-length
  this.headers[consts.CONTENT_LENGTH_HEADER] = this.body.length + content.length;

  // write version and status
  _head = 'HTTP/1.1 ' + this.status + ' ' + consts.STATUS_PHRASE[this.status] + consts.CRNL;

  // write headers
  for (var key in this.headers) {
    _head += key + ': ' + this.headers[key] + consts.CRNL;
  }
  _head += consts.CRNL;

  // write response data to socket - headers and body
  this.socket.write(_head, (function(self) {
    return function() {
			
			// write body to socket once headers writing is done
      self.socket.write(self.body, (function(self) {
        return function() {

					// continue to write body to socket once previous body writing is done
          self.socket.write(content, (function(self) {
            return function() {

              // if needs to close connection: end socket once all current writing is done
              if (self.closeConnection) {
                self.socket.end();
              }

            };
          })(self)); //write ender
        };
      })(self)); //write body
    };
  })(_this)); //write headers

  // callback to server with status
  if (this.counted) {
    this.counted = false;
    this.serverCB(this.status);
  }
}

exports.Response = Response;

