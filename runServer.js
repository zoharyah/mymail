var port = 1224,
		myHttp = require('./myHttp'),
    server = myHttp.createHTTPServer('./public'),
    fs = require('fs'),
    servConsts = require('./settings'),
    mailSettings = require('./mailSettings.js'),
    session = require('./session').s,
    uuid = require('node-uuid'),
    getCookie = require('./cookie').getRequestCookie,
    userDB = require('./myMailUserDB').userDB,
    io = require('socket.io').listen(port + 2);
    
/**
 * run the HTTP server
 */
server.start(port);

/**
 * "new user registration" request handler
 */
var respondToRegRequest = function respondToRegRequest (req, resp, params){
	// fetch resource or put login error on page
	// send cookie header for sessio managment
	var _user,
			_pass;

	clearTimeout(resp.timeout);
	
  // get registration params
	params = JSON.parse(req.body);
	
	// check that all required params are received
	if (!params.user || !params.pass || !params.firstName || !params.lastName || !params.age){
		resp.status = servConsts.STATUS_BAD_REQUEST;
    resp.end();
    return;
	}
	
	// create a new session for this user
	session.createSession(params.user, function(sessionID){
		
    if(sessionID){
      
      // if user registration was successful send a response with a sessionID cookie
			var regSucc = function regSucc(){
				// set user name and sessionID cookies in response
		    resp.headers['Set-Cookie'] = session.getSessionCookie('UserName', params.user).toString() +
																		'\nSet-Cookie: ' + session.getSessionCookie('sessionID', sessionID).toString();
				// tell client to redirect to mail app location
				resp.status = servConsts.STATUS_FOUND;
				resp.headers.Location = mailSettings.GLOBAL_MAIL_APP_PAGE_LOC;
				resp.end();
			};
			var regFail = function regFail(){
				resp.status = servConsts.STATUS_CONFLICT;
				resp.end();
			};
		  // add user to the database
			userDB.addUser(params, sessionID, regSucc, regFail);
		}
		// there was a problem creating a session - let the user know
		else{
			resp.status = servConsts.STATUS_BAD_REQUEST;
			resp.end();
		}
	});
}

/**
 * "login" request handler
 * if user made it here - he has no valid session 
 */
var respondToLoginRequest = function respondToLoginRequest (req, resp, params){
	var _user,
			_pass;
  
  clearTimeout(resp.timeout);
  // get login params
	params = JSON.parse(req.body);
	
	 // check that all required params are received
  if (!params.user || !params.pass){
		resp.status = servConsts.STATUS_BAD_REQUEST;
    resp.end();
    return;
	}
	
	// check if user is in database and verify password
	userDB.validateUser(params.user, params.pass, function(isValidUser){

    if(isValidUser){
      
		  // create a new session for this user
      session.createSession(params.user, function(sessionID){
				
				if(sessionID){
          // send a response with a sessionID cookie
			    resp.headers['Set-Cookie'] = session.getSessionCookie('UserName', params.user).toString() +
																			'\nSet-Cookie: ' + session.getSessionCookie('sessionID', sessionID).toString();
					// tell client to redirect to mail app location
					resp.status = servConsts.STATUS_FOUND;
					resp.headers.Location = mailSettings.GLOBAL_MAIL_APP_PAGE_LOC;
					resp.end();
				}
				else{
				  // couldn't create a session
					resp.status = servConsts.STATUS_BAD_REQUEST;
					resp.end();
				}
			});
		}
		else{
		  // not a valid user
			resp.status = servConsts.STATUS_BAD_REQUEST;
			resp.end();
		}
	});
}

/**
 * "fetch mail" request handler
 * get's all the email to and from the requesting user.
 */
var respondToFetchMail = function respondToFetchMail (req, resp, params){
	
	clearTimeout(resp.timeout);
	
	var username;
	
	if(!req.headers.Cookie){
			resp.status = servConsts.STATUS_BAD_REQUEST;
			resp.end();
			return;
	}
	
	// get username from cookie
	username = getCookie(req.headers.Cookie).UserName;
	
	// get user emails from database
	userDB.fetchUserMails(username,
		// success callback
		function(mailArr){
		  // if succesful - return mails array in responses body
			resp.body = '[' + mailArr.map(JSON.stringify).toString() + ']';
			resp.type = servConsts.extensionTypes['json'];
			resp.end();
		},
		// fail callback
		function(){
			resp.status = servConsts.STATUS_BAD_REQUEST;
			resp.end();
		}
	);
}

/**
 * "send mail" request handler
 */
var respondToSendMail = function respondToSendMail (req, resp, params){
	
	var mail,
			cookies;
			
	clearTimeout(resp.timeout);
	// make sure type is right and cookie available
	if (req.headers['Content-Type'].indexOf('application/json') !== 0 || !req.headers.Cookie){
		resp.status = servConsts.STATUS_BAD_REQUEST;
		resp.end();
		return;
	}
	
	// get mail content from request body
	mail = JSON.parse(req.body);
	
	// get username from cookie
	cookies = getCookie(req.headers.Cookie);
	
	var sendSucc = function regSucc(){
		resp.end();
	};
	var sendFail = function regFail(){
		// couldn't send mail - username doesn't exist
		resp.status = servConsts.STATUS_PRECONDITION_FAILED;
		resp.end();
	};
	
	// update database accordingly (both for sending and receiving users)
	userDB.sendMail(mail, cookies.UserName, sendSucc, sendFail);
	
	// extend user's session
  session.extendSession(cookies.sessionID);
}

/**
 * "delete mail" request handler
 */
var respondToDeleteMail = function respondToDeleteMail (req, resp, params){

	var cookies;
	
	clearTimeout(resp.timeout);
	
	// aint nothing I can do without a cookie
	if (!req.headers.Cookie){
		resp.status = servConsts.STATUS_BAD_REQUEST;
		resp.end();
		return;
	}
	
	// get cookie
	cookies = getCookie(req.headers.Cookie);
	
	// delete mail from database
	userDB.deleteMail(req.body, cookies.UserName);
		
	resp.end();
	
	// extend user's session
	session.extendSession(cookies.sessionID);
}

/**
 * "toggle read/unread" request handler
 */
var respondToToggleReadMail = function respondToToggleReadMail (req, resp, params){
	
	var cookies;
	
	clearTimeout(resp.timeout);
	
	// aint nothing I can do without a cookie
	if (!req.headers.Cookie){
		resp.status = servConsts.STATUS_BAD_REQUEST;
		resp.end();
		return;
	}
	
	// get cookie
	cookies = getCookie(req.headers.Cookie);
	
	// update mail status in database
	userDB.toggleReadMail(req.body, cookies.UserName);
	
	resp.end();
	
	// extend user's session
	session.extendSession(cookies.sessionID);
}

/**
 * respondWithLoginPage is the mail app page navigation helper 
 * if redirect is true - then the user will be redirected to 'redirPath'
 * otherwise - the user will get the page he requested according to appPage - if true then mail.html (main mail app page)
 */
var respondWithLoginPage = function respondWithLoginPage (resp, appPage, redirect, redirPath) {
	
	if(redirect){
		resp.status = servConsts.STATUS_FOUND;
		resp.headers.Location = redirPath;
		resp.end();
		return;
	}
	
	var pageLoc = appPage? mailSettings.MAIL_APP_PAGE_LOC : mailSettings.LOGIN_REG_PAGE_LOC;
	
	fs.readFile(pageLoc, function(err, data) {
		if (err) {

			//send error response
			console.log(err);
			resp.status = servConsts.STATUS_INTERNAL_SERVER_ERROR;
			resp.end();
			return;
		}

		//managed to read resource, sending it back to the client
		resp.status = servConsts.STATUS_OK;
		resp.headers[servConsts.CONTENT_TYPE_HEADER] = servConsts['.html'];
		resp.end(data);
	});
}

/**
 * "mail.html" page request handler
 * if an active session exists: redirect to mail app page
 */
var respondToMailAppPageReq = function respondToMailAppPageReq (req, resp, params){
	var cookie;
	clearTimeout(resp.timeout);
	
	// if there's no active session redirect to welcome page
	if(!req.headers.Cookie){
			respondWithLoginPage(resp, false, true, mailSettings.GLOBAL_LOGIN_REG_PAGE_LOC);
			return;
	}
	
	cookie = getCookie(req.headers.Cookie);
  
  // validate session
	session.isSessionValid(cookie.UserName, cookie.sessionID, 
		function(isValid){
		  // if not valid redirect to welcome page
			respondWithLoginPage(resp, true, !isValid, mailSettings.GLOBAL_LOGIN_REG_PAGE_LOC);
		}
	);
}

/**
 * "welcome.html" page request handler
 */
var respondToLoginRegPageReq = function respondToLoginRegPageReq (req, resp, params){
	var cookie;
	clearTimeout(resp.timeout);
	
	// if there's no active session send hime to welcome page
	if(!req.headers.Cookie){
			respondWithLoginPage(resp, false, false, mailSettings.GLOBAL_LOGIN_REG_PAGE_LOC);
			return;
	}
	
	cookie = getCookie(req.headers.Cookie);

  // validate session
	session.isSessionValid(cookie.UserName, cookie.sessionID, 
		function(isValid){
			// if isValid redirect to mail app page (no need to login/register)
			respondWithLoginPage(resp, false, isValid, mailSettings.GLOBAL_MAIL_APP_PAGE_LOC);
		});
}

/**
 * "probe" request handler - client is telling us to extend his session
 */
var respondToProbe = function respondToProbe (req, resp, params){
	var cookie;
	clearTimeout(resp.timeout);
	
	// if has no session active just redirect to welcome page
	if(!req.headers.Cookie){
			respondWithLoginPage(resp, false, true, mailSettings.GLOBAL_LOGIN_REG_PAGE_LOC);
			return;
	}
	resp.end();
	
	cookie = getCookie(req.headers.Cookie);
  // extend current session
	session.extendSession(cookie.sessionID);
	
}

/**
 * "end session" request handler
 */
var respondToEndSession = function respondToEndSession (req, resp, params){
	var cookie;
	clearTimeout(resp.timeout);
	
	// if has no session active just redirect to welcome page
	if(!req.headers.Cookie){
			respondWithLoginPage(resp, false, true, mailSettings.GLOBAL_LOGIN_REG_PAGE_LOC);
			return;
	}
	
	cookie = getCookie(req.headers.Cookie);
  // end current session
	session.endSession(cookie.sessionID);
	
	// redirect to welcome page
	respondWithLoginPage(resp, false, true, mailSettings.GLOBAL_LOGIN_REG_PAGE_LOC);
}


//===================================== DEBUG-MAINTENANCE-FUNCTIONS =========================================
//===========================================INTERNAL USE ONLY===============================================

var respondToDBStatusRequest = function respondToDBStatusRequest(req, resp, params){
  clearTimeout(resp.timeout);
	userDB.getDB(function(users){
		session.getSessionKeys(function(keys){
			resp.body = '<html><body>current mymail user db image:<br />';
			resp.body += '<table border="1" style="float:left"><tr><td>user</td><td>password</td><td>mails</td></tr>';
			for (var i in users)
				resp.body += '<tr><td>'+users[i].user+'</td><td>'+users[i].pass+'</td><td>'+JSON.stringify(users[i].mails).replace(/[,{}]/g, '<br />')+'</td></tr>';
			resp.body += '</table>';
			resp.body += '<table border="1"><tr><td>keys</td></tr>';
			for (var i in keys)
				resp.body += '<tr><td>'+keys[i]+'</td></tr>';
			resp.body += '</table></body></html>';
			 resp.type = servConsts.extensionTypes['html'];
			resp.end();
		});
	});
}

var respondToClearDBRequest = function respondToClearDBRequest(req, resp, params){
  clearTimeout(resp.timeout);
	userDB.clearDB();
	session.clearAllKeys();
	resp.body = 'OK! :)';
	resp.end();
}
//===================================== END_DEBUG-MAINTENANCE-FUNCTIONS =========================================

server.onStart(function(){
	
	console.log('server started.... on port: ' + port);
	
	// =======================================
	// 	register dynamic server functionality
	// =======================================
	
	server.post('/mymail/register', {call: respondToRegRequest});
	server.post('/mymail/login', {call: respondToLoginRequest});
	server.get('/mymail/fetchMail', {call: respondToFetchMail});
	server.post('/mymail/send', {call: respondToSendMail});
	server.post('/mymail/delete', {call: respondToDeleteMail});
	server.post('/mymail/toggleRead', {call: respondToToggleReadMail});
	
	server.get('/mail.html', {call: respondToMailAppPageReq});
	server.get('/welcome.html', {call: respondToLoginRegPageReq});
		
	server.post('/mymail/probe', {call: respondToProbe});
	server.post('/mymail/endSession', {call: respondToEndSession});
	
	// production phase debug functions - not safe for final product
	// server.get('/mymail/dbstatus', {call: respondToDBStatusRequest});
	// server.get('/mymail/clear', {call: respondToClearDBRequest});
	
	
	// =======================================
	// 	setup socket.io
	// =======================================

	// decrease socket.io verbosity - its loud
	io.set('log level', 1);
	
	// handle socket.io clients
	io.sockets.on('connection', function(socket){
		
		var userName;
				
	  socket.on('startSession', function (clientSession) {
	    userName = clientSession.UserName;
	    if(!userName){
	    	console.log('socket.io got a defective username');
	    }
	    else {
	    	// save user socket for later communications
		    userDB.userSockets[userName] = socket;
		  }
	  });
	  socket.on('disconnect', function () {
	  	// delete user socket
	  	delete userDB.userSockets[userName];
	  });
	});
});
