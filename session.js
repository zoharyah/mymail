var cookie = require('./cookie'),
		s = require("./mailSettings"),
		uuid = require('node-uuid'),
		redis = require("redis");

// ====================================================SESSION====================================================================

/**
 * SessionManager constructor 
 */
function SessionManager(){
	this.client = redis.createClient(6379);
}

/**
 * createSession(user, callback) creates a new session for user
 * if successful calls callback with sessionID, otherwise - calls callback with false
 * 
 * @param {Object} user
 * @param {Object} callback
 */
SessionManager.prototype.createSession = function createSession (user, callback){
	var _this = this,
			id = uuid();
			
	// create new session in DB
	this.client.set(id, user, function(err, result){
		if(err){
			console.log(err);
			callback(false);
		}
		else{
			// set expiration for this session according to mailSettings - and call callback with new id afterwards
			_this	.client.expire(id, s.SESSION_TIMEOUT_SECS, function(err, result){ //expire time is in seconds
				callback(id);
			});
		}
	});
}

/**
 * isSessionValid(user, sessionID, callback)
 * calls callback with true if session with sessionID for username user is valid
 * otherwise - calls callback with false
 * 
 * @param {Object} user
 * @param {Object} sessionID
 * @param {Object} callback
 */
SessionManager.prototype.isSessionValid = function isSessionValid (user, sessionID, callback){
	
	var _this = this;

	this.client.get(sessionID, function (err, reply) {
		if (err){
			callback(false);
		}
    else if(reply) {
        if(reply === user){
        	// extend session timeout
        	_this.client.expire(sessionID, s.SESSION_TIMEOUT_SECS);
        	callback(true);
        }
        else{
        	callback(false);
        }
    } else {
      	callback(false);
    }
  });
}

/**
 * extendSession(sessionID)
 * extends user session for session with session id sessionID
 *  
 * @param {Object} sessionID
 */
SessionManager.prototype.extendSession = function extendSession (sessionID){
	this.client.expire(sessionID, s.SESSION_TIMEOUT_SECS);
}

/**
 * endSession(sessionID)
 * deletes the session with session id sessionID from the sessions DB
 *  
 * @param {Object} sessionID
 */
SessionManager.prototype.endSession = function endSession (sessionID){
	this.client.del(sessionID);
}

/**
 * getSessionCookie(name, val)
 * returns a Cookie object with name and value val
 *  
 * @param {Object} name
 * @param {Object} val
 */
SessionManager.prototype.getSessionCookie = function getSessionCookie (name, val) {
	var sessionCookie = new cookie.Cookie(name, val, new Date((new Date()).getTime() + s.COOKIE_EXPIRATION).toGMTString());
	return sessionCookie;
}


// =====================================DEBUG=======================================

SessionManager.prototype.getSessionKeys = function getSessionKeys (callback){
	this.client.keys('*', function (err, keys) {
	  if (err) {
	  	console.log(err);
	  	callback([]);
	  }
		else{
		  callback(keys);
		}
	});
}

SessionManager.prototype.clearAllKeys = function clearAllKeys () {
	var _this = this;
	this.getSessionKeys(function(keys){
		for(var i in keys){
			_this.client.del(keys[i]);
		}
	});
}
// =====================================END_DEBUG=======================================


exports.s = new SessionManager();
