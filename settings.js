/**
 * constatns for myHttp server 
 */

exports.RESPONSE_END_TIMEOUT = 10000;
exports.SECOND = 1000;
exports.LAST_REQUEST_TIMEOUT_SEC = 10;
exports.CRNL = '\r\n';
exports.END_OF_HEADERS = exports.CRNL + exports.CRNL;
exports.BREAK_LINE_REGEX = /\n|\r\n/;

exports.GET_METHOD = 'GET';
exports.POST_METHOD = 'POST';

exports.CONTENT_LENGTH_HEADER = 'Content-Length';
exports.CONTENT_TYPE_HEADER = 'Content-Type';
exports.CONNECTION_HEADER = 'connection';

exports.STATUS_OK = 200;
exports.STATUS_FOUND = 302;
exports.STATUS_BAD_REQUEST = 400;
exports.STATUS_NOT_FOUND = 404;
exports.STATUS_METHOD_NOT_ALLOWED = 405;
exports.STATUS_TIME_OUT = 408;
exports.STATUS_CONFLICT = 409;
exports.STATUS_PRECONDITION_FAILED = 412;
exports.STATUS_INTERNAL_SERVER_ERROR = 500;
exports.STATUS_SERVICE_UNAVAILABLE = 503;

exports.UNLIMITED_LISTENERS = 0;

exports.STATUS_PHRASE = {
    200: 'OK',
    302: 'Found',
    400: 'Bad Request',
    404: 'File Not Found',
    503: 'Service Unavailable',
    405: 'Method Not Allowed',
		408: 'Request Timeout',
		409: 'Conflict',
		412: 'Precondition Failed',
    500: 'Internal Server Error'
};

exports.extensionTypes = {
		'.js'		:'application/javascript',
		'.html'	:'text/html',
		'.css'	:'text/css',
		'.gif'	:'image/gif',
		'.jpeg'	:'image/jpeg',
		'.jpg'	:'image/jpeg',
		'.png'	:'image/png',
		'.json'	:'application/json',
		'.ico'	:'image/x-icon'
};