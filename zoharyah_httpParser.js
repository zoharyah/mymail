var consts = require('./settings');
/**
 * a pretty straight forward constructor for our HttpRequest.
 * param method is the request method as string
 * param uri is the request uri as string (e.g. /dir1/dir2/king.html)
 * param resource is the requested resource as string (e.g. king.html)
 * param resPath is the path to the resource (e.g. /dir1/dir2)
 * param headers is a key/val hash of: <header_name>:<header_val>
 * param body is the body of the request
 */
var HttpRequest = function HttpRequest(method, uri, resource, resPath, headers, body, version) {
	this.method = method;
	this.uri = uri;
	this.resource = resource;
	this.resPath = resPath;
	this.headers = headers;
	this.version = version;
	this.body = body;
}


/**
 * parsing a HttpRequest String into an HttpRequest Object. Will return an empty Object if any
 * of the input is illegal
 * param requestString a String representation of an HttpRequest
 */
var parseRequest = function parseRequest(requestString) {
	var re,
			resourceIndex,
			name,
			value,
			headers = {},
			method = '',
			uri = '',
			resource = '',
			resPath = '',
			version = '',
			body = '',
			startIndex = 0,
			character = 0;
			
	// check if input is defined and is a String
	if (!requestString || (typeof requestString !== 'string' && !(requestString instanceof String))){
		return {status: consts.STATUS_BAD_REQUEST};
	}
	
	//============================parse first line of request==========================================
	
	// copy method from input request
	while(character < requestString.length && requestString[character] !== ' ' && requestString[character] !== '\n' && requestString[character] !== '\r'){
		method += requestString[character];
		character++;
	}
	
	// validate method - it should be either POST or GET
	if(method !== 'GET' && method !== 'POST'){ 
		return {status: consts.STATUS_METHOD_NOT_ALLOWED};
	}
	
	// discard trailing spaces
	while(character < requestString.length && requestString[character] === ' '){ 
		character++;
	}
	// copy URI
	while(character < requestString.length && requestString[character] !== ' ' && requestString[character] !== '\n' && requestString[character] !== '\r'){
		uri += requestString[character];
		character++;
	}
	// find position of resource (after last /)
	resourceIndex = uri.lastIndexOf('/');
	
	// if URI doesn't contain '/' - this is not a valid URI - return empty object
	if(resourceIndex < 0){
		return {status: consts.STATUS_BAD_REQUEST};
	}
	
	// seperate and save resource
	resource = uri.substr(resourceIndex);
	
	// seperate and save resource path
	resPath = uri.substr(0, resourceIndex);
	
	// discard trailing spaces
	while(character < requestString.length && requestString[character] === ' '){
		character++;
	}
	// get HTTP version
	while(character < requestString.length && requestString[character] !== '\n' && requestString[character] !== '\r'){
		version += requestString[character];
		character++;
	}
	//make sure we've reached end of line
	if (requestString[character] !== '\n' && ++character < requestString.length && requestString[character] !== '\n'){
		// missing end of line after version
		return {status: consts.STATUS_BAD_REQUEST}; 
	}
	//regular expression to describe legal http version string
	re = new RegExp('( )*HTTP/[0-9]+\.[0-9]+( )*');
	
	// check validality of version format
	if(!version.match(re)){
		return {status: consts.STATUS_BAD_REQUEST};
	}
	// ignore newline and/or spaces
	 while(character < requestString.length && requestString[character] === ' '){ 
		character++;
	}
	
	//currently at newline character, continue to next line
	character++;
	
	//if we've reached end of string - this request is legal but has no headers or body, return new HttpRequest
	if(character >= requestString.length){
		return new HttpRequest(method, uri, resource, resPath, headers, body);
	}
	
	//============================parse request headers==========================================
	
	// loop over all headers untill you get 2 consecutive \r\n's
	while(character < requestString.length && requestString[character] !== '\n' && requestString[character] !== '\r'){		
		
		// reset namd and value for next header
		name = '';
		value = '';
		
		// copy header's key
		while(character < requestString.length && requestString[character] !== ':' && requestString[character] !== '\r' && requestString[character] !== '\n'){ 
			name += requestString[character];
			character++;
		}
		//validate header format
		if (requestString[character] !== ':') { 
			return {status: consts.STATUS_BAD_REQUEST};
		}
		// skip ':'
		character++; 
		// copy header's value
		while(character < requestString.length && requestString[character] !== '\n' && requestString[character] !== '\r'){
			value += requestString[character];
			character++;
		}
		if (requestString[character] !== '\n' && ++character < requestString.length && requestString[character] !== '\n'){
			// missing end of line after header's value
			return {status: consts.STATUS_BAD_REQUEST}; 
		}
		headers[name] = value.trim();
		
		// currently at newline character, continue to next line
		character++;
	}
	
	// check if there's an empty line after headers (and advance character)
	if (character < requestString.length && requestString[character] !== '\n' && ++character < requestString.length && requestString[character] !== '\n'){
		// missing end of line after headers
		return {status: consts.STATUS_BAD_REQUEST}; 
	}
	
	// ignore newline and/or spaces
	 while(character < requestString.length && (requestString[character] === ' ' || requestString[character] === '\r' || requestString[character] === '\n')){ 
		character++;
	}
	
	//============================get request body==========================================
	
	// get body
	if(character < requestString.length){
		body = requestString.substring(character);
	}
	
	// if we got here, input was OK, create HttpRequest Object
	return new HttpRequest(method, uri, resource, resPath, headers, body, version);
}

module.exports = parseRequest;

/* this was our first approach at the above function, it was discarded for  performance optimization
var parseRequest = function parseRequest(requestString) {
	var lines, 		// seperate request into lines
			currLine, // first line is the method line (<METHOD> <URI> HTTP/<VER>) seperate into elements
			method,
			uri,
			resource,
			resPath,
			headers,
			body,
			i,
			re,
			resourceIndex;

	// when not getting request, or getting an empty request return an empty Object
	if (!requestString){
		return {};
	}

	//this is a regular expression to define a legal request line for this parser, to be matched with first line of input
	re = new RegExp('(POST|GET)( )+.*.*( )+HTTP/[0-9]\.[0-9]');

	lines = requestString.split('\r\n');

	//lines[0] is the request line, if we pass this condition, it is legal for our HttpRequest parser
	if (!lines[0].match(re)){
		return {};
	}

	currLine = lines[0].trim().split(/ +/);
	body = "";

	// METHOD is the first element of the line
	method = currLine[0];
	
	// URI is the second element
	uri = currLine[1];
	
	// find position of resource (after last /)
	resourceIndex = uri.lastIndexOf('/');
	
	// seperate RESOURCE
	resource = uri.substr(resourceIndex + 1);
	
	// seperate RES_PATH
	resPath = uri.substr(0, resourceIndex);

	// init empy hash for headers
	headers = {};
	// for every line until an empty line (where the body begines)
	for ( i = 1; i < lines.length && lines[i] !== ''; i++ ) {
		// seperate hash key from val (they are seperated by ': ')
		currLine = lines[i].split(/: /);
		// if missing val return an empty Object
		if (currLine.length < 2){
			return {};
		}
		// add tuple to headers - if currLine.length > 2 then join it to one string as it was before the split
		headers[currLine[0]] = currLine.slice(1).join(':');
	}

	// join body with '\n' that we used before to seperate, body is in lines starting from i+1
	body = lines.slice(i+1).join('\r\n');
	
	// if we got here, input was OK, create HttpRequest Object
	return new HttpRequest(method, uri, resource, resPath, headers, body);
}
*/